# cub3D

## About

School 42 project built from scratch in C. \
This is a small game reproducing a maze in a 3D like environment with raycasting techniques. \
It's working on UNIX environments.

![](images/cub3D.gif)

***

## How to run it

If you're using Linux, replace in Makefile line 20 "MLXFLAGS= -lmlx -lX11 -lXext" \
You'll also probably need to install libX11-devel library

```
git clone https://gitlab.com/Ralphiki/cub3d.git
cd cub3D
make
./cub3D maps/map.cub
```

***

## Map edition example

### maps/map.cub

```
NO	./textures/north.xpm 
SO  ./textures/south.xpm 
WE  ./textures/west.xpm
EA  ./textures/east.xpm

=> change texture file above for each direction

F 25, 0, 215
C 10, 25, 16

=> change floor (F) and ceiling (C) RGB color

11111111
10000001
10010001
100N1001
10010001
10000001
11111111

=> map structure : 0 for free area, 1 for a wall, N for player
```

## Play 

Use WASD to move the character \
Use left and right arrow keys to rotate
