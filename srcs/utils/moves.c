/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   moves.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vmasse <vmasse@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/29 16:57:35 by mlazzare          #+#    #+#             */
/*   Updated: 2022/06/30 12:50:14 by vmasse           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/cub3d.h"

void	move_leftright(t_game *game)
{
	if (game->move.left)
	{
		if (game->config.maze[(int)((game->player.posx) - game->player.diry
				* (SPEED + 0.1))][(int)(game->player.posy)] == '0')
			game->player.posx -= game->player.diry * SPEED;
		if (game->config.maze[(int)game->player.posx][(int)(game->player.posy
			+ game->player.dirx * (SPEED + 0.1))] == '0')
			game->player.posy += game->player.dirx * SPEED;
	}
	if (game->move.right)
	{
		if (game->config.maze[(int)((game->player.posx) + game->player.diry
				* (SPEED + 0.1))][(int)(game->player.posy)] == '0')
			game->player.posx += game->player.diry * SPEED;
		if (game->config.maze[(int)game->player.posx][(int)(game->player.posy
			- game->player.dirx * (SPEED + 0.1))] == '0')
			game->player.posy -= game->player.dirx * SPEED;
	}
}

void	move_backforwards(t_game *game)
{
	if (game->move.forw)
	{
		if (game->config.maze[(int)(game->player.posx + game->player.dirx
				* (SPEED + 0.1))][(int)(game->player.posy)] == '0')
			game->player.posx += game->player.dirx * SPEED;
		if (game->config.maze[(int)game->player.posx][(int)(game->player.posy
			+ game->player.diry * (SPEED + 0.1))] == '0')
			game->player.posy += game->player.diry * SPEED;
	}
	if (game->move.back)
	{
		if (game->config.maze[(int)((game->player.posx) - game->player.dirx
				* (SPEED + 0.1))][(int)((game->player.posy))] == '0')
			game->player.posx -= game->player.dirx * SPEED;
		if (game->config.maze[(int)game->player.posx][(int)(game->player.posy
			- game->player.diry * (SPEED + 0.1))] == '0')
			game->player.posy -= game->player.diry * SPEED;
	}
}

void	rotate_view(t_game *game, double dx, double px)
{
	if (game->move.rotright && game->move.rotleft)
		return ;
	if (game->move.rotright)
	{
		game->player.dirx = game->player.dirx * cos(-ROT)
			- game->player.diry * sin(-ROT);
		game->player.diry = dx * sin(-ROT)
			+ game->player.diry * cos(-ROT);
		game->player.planex = game->player.planex * cos(-ROT)
			- game->player.planey * sin(-ROT);
		game->player.planey = px * sin(-ROT) + game->player.planey
			* cos(-ROT);
	}
	if (game->move.rotleft)
	{
		game->player.dirx = game->player.dirx * cos(ROT)
			- game->player.diry * sin(ROT);
		game->player.diry = dx * sin(ROT) + game->player.diry
			* cos(ROT);
		game->player.planex = game->player.planex * cos(ROT)
			- game->player.planey * sin(ROT);
		game->player.planey = px * sin(ROT) + game->player.planey
			* cos(ROT);
	}
}

int	key_press(int keycode, t_game *game)
{
	if (keycode == RIGHT_KEY)
		game->move.rotright = 1;
	if (keycode == LEFT_KEY)
		game->move.rotleft = 1;
	if (keycode == W_KEY)
		game->move.forw = 1;
	if (keycode == A_KEY)
		game->move.left = 1;
	if (keycode == S_KEY)
		game->move.back = 1;
	if (keycode == D_KEY)
		game->move.right = 1;
	if (keycode == ESC_KEY)
	{
		free_game(game);
		exit(0);
	}
	return (1);
}

int	key_release(int keycode, t_game *game)
{
	if (keycode == W_KEY)
		game->move.forw = 0;
	if (keycode == A_KEY)
		game->move.left = 0;
	if (keycode == S_KEY)
		game->move.back = 0;
	if (keycode == D_KEY)
		game->move.right = 0;
	if (keycode == RIGHT_KEY)
		game->move.rotright = 0;
	if (keycode == LEFT_KEY)
		game->move.rotleft = 0;
	return (1);
}
