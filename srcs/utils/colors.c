/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vmasse <vmasse@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/27 14:55:39 by mlazzare          #+#    #+#             */
/*   Updated: 2022/07/16 16:01:20 by vmasse           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/cub3d.h"

int	get_colors(t_map *map, char c)
{
	if (c == 'c')
		return (map->c[0] << 16 | map->c[1] << 8 | map->c[2]);
	return (map->f[0] << 16 | map->f[1] << 8 | map->f[2]);
}

void	colorize_environment(int i, t_game *game)
{
	int	j;

	j = -1;
	while (++j < game->wall.start)
		game->img.addr[j * game->img.line_length / 4 + i]
			= get_colors(&game->config, 'c');
	paint_wall(game, i, &j, TEX);
	while (++j < game->win_h)
		game->img.addr[j * game->img.line_length / 4 + i]
			= get_colors(&game->config, 'f');
}
