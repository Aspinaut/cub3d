/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vmasse <vmasse@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/29 12:54:30 by mlazzare          #+#    #+#             */
/*   Updated: 2022/07/16 16:10:28 by vmasse           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/cub3d.h"

int	get_error(char c)
{
	if (c == 'r' || c == 't')
		printf("Error\nInvalid file path or texture\n");
	else if (c == 'o' )
		printf("Error\nFile doesn't exist or permission missing\n");
	else if (c == 'l')
		printf("Error\nAn error occurred when reading the file\n");
	else if (c == 'g')
		printf("Error\nInvalid rgba configuration\n");
	else if (c == 'd')
		printf("Error\nInvalid direction configuration\n");
	else if (c == 'e')
		printf("Error\nInvalid file extension\n");
	else if (c == 'x')
		printf("Error\nInvalid value declaration in config\n");
	else if (c == 'm' || c == 'w')
		printf("Error\nInvalid map\n");
	else if (c == 'p')
		printf("Error\nInvalid player position\n");
	else if (c == 'y')
		printf("Error\nAn error occurred in mlx library\n");
	else if (c == 'f')
		printf("Error\nToo many or too few values in map config\n");
	else if (c == 'i')
		printf("Error\nConfig file empty\n");
	return (1);
}

int	free_line(char *l)
{
	if (l)
		free(l);
	l = NULL;
	return (0);
}

int	check_err(int err, int space, t_map *map)
{
	if (!err && !map->complete)
		return (get_error('i'));
	if (!err && !map->idx)
		return (get_error('w'));
	if (!err && !space && check_bottom_wall(map, map->maze[map->idx - 1]))
		return (get_error('w'));
	return (err);
}
