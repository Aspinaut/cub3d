/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vmasse <vmasse@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/02 17:25:03 by mlazzare          #+#    #+#             */
/*   Updated: 2022/06/30 12:48:07 by vmasse           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/cub3d.h"

int	close_window(t_game *game)
{
	free_game(game);
	exit(0);
	return (0);
}

void	free_map(t_map *map)
{
	int	i;

	i = -1;
	if (map->no)
		free(map->no);
	if (map->so)
		free(map->so);
	if (map->we)
		free(map->we);
	if (map->ea)
		free(map->ea);
	if (map->s)
		free(map->s);
	while (map->maze && map->maze[++i])
		free(map->maze[i]);
	if (map->maze)
		free(map->maze);
	free(map);
}

void	free_game(t_game *game)
{
	int	i;

	i = -1;
	while (++i < 4)
		if (game->tex[i].img)
			mlx_destroy_image(game->mlx, game->tex[i].img);
	if (game->img.img)
		mlx_destroy_image(game->mlx, game->img.img);
	if (game->img.img2)
		mlx_destroy_image(game->mlx, game->img.img2);
	if (game->win)
		mlx_destroy_window(game->mlx, game->win);
	game->win = 0;
}

int	free_window(t_game *game)
{
	int	i;

	i = -1;
	printf("Error\nTexture file failed to open\n");
	while (++i < 4)
		if (game->tex[i].img)
			mlx_destroy_image(game->mlx, game->tex[i].img);
	if (game->win)
		mlx_destroy_window(game->mlx, game->win);
	free(game->mlx);
	return (0);
}

int	free_dataimg(t_game *game)
{
	int	i;

	i = -1;
	printf("Error\nImage data failed to initialise\n");
	while (++i < 4)
		if (game->tex[i].img)
			mlx_destroy_image(game->mlx, game->tex[i].img);
	if (game->img.img)
		mlx_destroy_image(game->mlx, game->img.img);
	if (game->img.img2)
		mlx_destroy_image(game->mlx, game->img.img2);
	if (game->win)
		mlx_destroy_window(game->mlx, game->win);
	game->win = 0;
	free(game->mlx);
	return (0);
}
