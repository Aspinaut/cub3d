/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycasting.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vmasse <vmasse@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/20 10:55:57 by vmasse            #+#    #+#             */
/*   Updated: 2022/06/29 17:20:32 by vmasse           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/cub3d.h"

static void	calculate_wall_values(t_game *game)
{
	if (game->side.side == 0)
		game->wall.distance = game->side.x - game->side.deltax;
	else
		game->wall.distance = game->side.y - game->side.deltay;
	game->wall.height = (int)(game->win_h / game->wall.distance);
	game->wall.start = -game->wall.height / 2 + game->win_h / 2;
	if (game->wall.start < 0)
		game->wall.start = 0;
	game->wall.end = game->wall.height / 2 + game->win_h / 2;
	if (game->wall.end >= game->win_h)
		game->wall.end = game->win_h - game->wall.start;
}

static int	is_hit_by_wall(t_game *game)
{
	if (game->side.x < game->side.y)
	{
		game->side.x += game->side.deltax;
		game->side.mapx += game->side.stepx;
		game->side.side = 0;
	}
	else
	{
		game->side.y += game->side.deltay;
		game->side.mapy += game->side.stepy;
		game->side.side = 1;
	}
	if (game->config.maze[game->side.mapx][game->side.mapy] == '1')
		return (1);
	return (0);
}

static void	calculate_side_values(t_game *game)
{
	if (game->player.rayx < 0)
	{
		game->side.stepx = -1;
		game->side.x = (game->player.posx - game->side.mapx)
			* game->side.deltax;
	}
	else
	{
		game->side.stepx = 1;
		game->side.x = (game->side.mapx + 1.0 - game->player.posx)
			* game->side.deltax;
	}
	if (game->player.rayy < 0)
	{
		game->side.stepy = -1;
		game->side.y = (game->player.posy - game->side.mapy)
			* game->side.deltay;
	}
	else
	{
		game->side.stepy = 1;
		game->side.y = (game->side.mapy + 1.0 - game->player.posy)
			* game->side.deltay;
	}
}

static void	calculate_ray_values(t_game *game, int x)
{
	game->side.camx = (double)2 * x / game->win_w - 1;
	game->player.rayx = game->player.dirx + game->player.planex
		* game->side.camx;
	game->player.rayy = game->player.diry + game->player.planey
		* game->side.camx;
	game->side.mapx = game->player.posx;
	game->side.mapy = game->player.posy;
	if (game->player.rayx == 0.0)
		game->side.deltax = 1e30;
	else
		game->side.deltax = fabs(1.0 / (double)game->player.rayx);
	if (game->player.rayy == 0.0)
		game->side.deltay = 1e30;
	else
		game->side.deltay = fabs(1.0 / (double)game->player.rayy);
}

void	draw_3d(t_game *game)
{
	int	x;
	int	hit;

	x = -1;
	while (++x < game->win_w)
	{
		calculate_ray_values(game, x);
		calculate_side_values(game);
		hit = 0;
		while (hit == 0)
			hit = is_hit_by_wall(game);
		calculate_wall_values(game);
		colorize_environment(x, game);
	}
	mlx_put_image_to_window(game->mlx, game->win, game->img.img, 0, 0);
}
