/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   start_game.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vmasse <vmasse@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/19 12:41:56 by mlazzare          #+#    #+#             */
/*   Updated: 2022/06/29 17:20:32 by vmasse           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/cub3d.h"

static int	game_loop(t_game *game)
{	
	if (game->win)
	{
		draw_3d(game);
		move_leftright(game);
		move_backforwards(game);
		rotate_view(game, game->player.dirx, game->player.planex);
		return (0);
	}
	return (-1);
}

static int	set_window(t_game *game)
{
	game->win = mlx_new_window(game->mlx, game->win_w,
			game->win_h, "Cub3D");
	if (!game->win)
		return (free_window(game));
	game->img.img = mlx_new_image(game->mlx, game->win_w,
			game->win_h);
	game->img.addr = (int *)mlx_get_data_addr(game->img.img,
			&game->img.bits_per_pixel,
			&game->img.line_length, &game->img.endian);
	game->img.img2 = mlx_new_image(game->mlx, game->win_w,
			game->win_h);
	game->img.addr2 = (int *)mlx_get_data_addr(game->img.img2,
			&game->img.bits_per_pixel,
			&game->img.line_length, &game->img.endian);
	game->img.counter = 0;
	if (!game->img.img || !game->img.img2
		|| !game->img.addr || !game->img.addr2)
		return (free_dataimg(game));
	return (1);
}

int	start_game(t_map map)
{
	int		g;
	t_game	game;

	g = init_game(&game, map);
	if (!g)
		return (get_error('y') - 1);
	game.mlx = mlx_init();
	if (!game.mlx)
		return (get_error('y') - 1);
	if (!texture_to_img(&game) || !set_window(&game))
		return (0);
	mlx_loop_hook(game.mlx, game_loop, &game);
	mlx_hook(game.win, 2, 1L << 0, key_press, &game);
	mlx_hook(game.win, 3, 1L << 1, key_release, &game);
	mlx_hook(game.win, 17, 1L << 17, close_window, &game);
	mlx_loop(game.mlx);
	mlx_destroy_window(game.mlx, game.win);
	free(game.mlx);
	return (1);
}
