/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_game.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vmasse <vmasse@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/30 09:35:28 by mlazzare          #+#    #+#             */
/*   Updated: 2022/06/29 17:20:32 by vmasse           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/cub3d.h"

static void	init_texture(t_game *game)
{
	int	j;

	j = -1;
	while (++ j < 4)
	{
		game->tex[j].img = 0;
		game->tex[j].addr = 0;
		game->tex[j].height = 0;
		game->tex[j].width = 0;
	}
	game->t.dir = 0;
	game->t.wallx = 0;
	game->t.x = 0;
	game->t.y = 0;
	game->t.height = 0;
	game->t.width = 0;
	game->t.initpos = 0;
	game->t.step = 0;
}

static void	init_wallside(t_game *game)
{
	game->wall.distance = 0;
	game->wall.height = 0;
	game->wall.start = 0;
	game->wall.end = 0;
	game->side.x = 0;
	game->side.y = 0;
	game->side.deltax = 0;
	game->side.deltay = 0;
	game->side.stepx = 0;
	game->side.stepy = 0;
	game->side.side = 0;
	game->side.camx = 0;
	game->side.mapx = 0;
	game->side.mapy = 0;
}

int	init_game(t_game *game, t_map map)
{
	game->config = map;
	game->win_w = SCREENW;
	game->win_h = SCREENH;
	game->player.posx = game->config.x + 0.5;
	game->player.posy = game->config.y + 0.5;
	game->player.rayx = -1;
	game->player.rayy = 0;
	game->move.forw = 0;
	game->move.back = 0;
	game->move.right = 0;
	game->move.left = 0;
	game->move.rotright = 0;
	game->move.rotleft = 0;
	get_orientation(game);
	init_wallside(game);
	init_texture(game);
	return (1);
}
