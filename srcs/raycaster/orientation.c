/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   orientation.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vmasse <vmasse@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/27 15:14:43 by vmasse            #+#    #+#             */
/*   Updated: 2022/06/27 15:15:04 by vmasse           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/cub3d.h"

static void	update_orientation(t_game *game, char pos)
{
	if (pos == 'N' || pos == 'S')
	{
		game->player.diry = 0;
		game->player.planex = 0;
	}
	else
	{
		game->player.dirx = 0;
		game->player.planey = 0;
	}
	game->config.maze[game->config.x][game->config.y] = '0';
}

void	get_orientation(t_game *game)
{
	char	p;

	p = game->config.maze[game->config.x][game->config.y];
	if (p == 'N')
	{
		game->player.dirx = -FOV;
		game->player.planey = 0.66;
	}
	else if (p == 'S')
	{
		game->player.dirx = FOV;
		game->player.planey = -0.66;
	}
	else if (p == 'E')
	{
		game->player.diry = FOV;
		game->player.planex = 0.66;
	}
	else if (p == 'W')
	{
		game->player.diry = -FOV;
		game->player.planex = -0.66;
	}
	update_orientation(game, p);
}
