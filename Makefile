# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vmasse <vmasse@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/04/19 08:53:28 by mlazzare          #+#    #+#              #
#    Updated: 2022/06/30 12:54:48 by vmasse           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = cub3D

CC = gcc
CFLAGS = -Wall -Wextra -Werror

LIB = -L./libft -L./mlx -lm

MLXFLAGS = -framework OpenGL -framework AppKit

HEADER = -I./includes

SRCS =  \
	    ./srcs/parser/parser.c \
    	./srcs/parser/parse_config.c \
		./srcs/parser/parse_config_utils.c \
    	./srcs/parser/parse_maze.c \
		./srcs/parser/parse_maze_utils.c \
		./srcs/parser/parse_check_walls.c \
		./srcs/parser/parser_utils.c \
		./srcs/raycaster/start_game.c \
		./srcs/raycaster/raycasting.c \
		./srcs/raycaster/orientation.c \
		./srcs/raycaster/init_game.c \
		./srcs/raycaster/textures.c \
		./srcs/utils/free.c \
		./srcs/utils/errors.c \
		./srcs/utils/colors.c \
		./srcs/utils/moves.c \
		./srcs/main.c

OBJS = 	$(SRCS:.c=.o)
MLX = ./mlx/libmlx.a
LIBFT = libft/libft.a

$(NAME): $(LIBFT) $(MLX) $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) $(MLXFLAGS) $(MLX) $(LIB) $(HEADER) -lft -o $(NAME)

all:$(NAME)
	
$(LIBFT):
	$(MAKE) -C ./libft

$(MLX):
	$(MAKE) -C ./mlx
		
clean:
	@rm -f $(OBJS)
	@cd libft && make clean
	@cd mlx && make clean

fclean:	clean
	@rm -rf $(NAME)
	@cd libft && make fclean

re:	fclean all

.PHONY:	all clean fclean re
